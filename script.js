const movies = [
  {
    title: "Avatar",
    releaseYear: 2009,
    duration: 162,
    director: "James Cameron",
    actors: ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
    description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
    poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
    rating: 7.9,
  },
  {
    title: "300",
    releaseYear: 2006,
    duration: 117,
    director: "Zack Snyder",
    actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
    description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
    poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
    rating: 7.7,
  },
  {
    title: "Avengers",
    releaseYear: 2012,
    duration: 143,
    director: "Joss Whedon",
    actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
    description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
    poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
    rating: 8.1,
  },
];

// Affichage la liste des films dans la console
console.log(movies)

// Récupération de la section qui va contenir les films
const movieSection = document.querySelector('.movie-section');

// Création fonction qui affiche la liste des films
function displayMovies() {
  for (const currentMovie of movies) {
    // Création de la <div> qui contiendra les informations du film
    const container = document.createElement('div');
    // Ajout du style à <div>
    container.classList.add("divStyle");
    // Ajout d'une classe 
    // container.classList.add("")
    // Insertion de <div> dans la <section>
    movieSection.append(container);

    // Création de la <h3> qui contiendra le titre
    const title = document.createElement('h3');
    // Insertion de <h3> dans la <div>
    container.append(title);
    // Assignation du titre à <h3>
    title.innerHTML = currentMovie.title;

    const poster = document.createElement('img');
    poster.classList.add("resizeImage");
    poster.src = currentMovie.poster;
    container.append(poster)

    const release = document.createElement("p");
    release.textContent = "Date de sortie : " + currentMovie.releaseYear;
    container.append(release);

    const duration = document.createElement('p');
    duration.textContent = "Durée : " + currentMovie.duration + " minutes";
    container.append(duration);

    const director = document.createElement('p');
    director.textContent = "Réalisateur : " + currentMovie.director;
    container.append(director);

    const actors = document.createElement('ul');
    actors.textContent = "Acteurs :"
    for (person of currentMovie.actors) {
        li = document.createElement('li')
        li.innerHTML = person;
        actors.append(li);
    };
    container.append(actors)

    const description = document.createElement('p');
    description.innerHTML = "Synopsis : " + currentMovie.description;
    container.append(description);

    const rating = document.createElement('p');
    rating.innerHTML = "Note : " + currentMovie.rating;
    container.append(rating);

    const btnDelete = document.createElement('button')
    btnDelete.textContent = "Supprimer";
    btnDelete.setAttribute("type", "button");
    btnDelete.classList.add(currentMovie.title);
    btnDelete.addEventListener('click', () => {
      container.innerHTML = "";
    } )
    container.append(btnDelete);
};
}

// Récupération le formulaire
const form = document.querySelector('form')

// Création la fonction SubmitForm qui enregistrera les valeurs dans un nouvel objet
function submitForm (event) {
  // Empêche le rafraîchessement de la page au submit
  event.preventDefault();
  // Création d'un nouvel objet dans le tableau movies
  let movie = {
    title: "",
    releaseYear: 0,
    duration: 0,
    director: "",
    actors: [],
    description: "",
    poster: "",
    rating: 0,
  };
  // Ajout de l'objet dans le tableau movies
  movies.push(movie);
  // Affectation des valeurs issues de l'input dans les propriétés de l'objet
  const titleInput = document.getElementById('title');
  movie.title = titleInput.value;
  const dateInput = document.getElementById('date');
  movie.releaseYear = dateInput.value;
  const durationInput = document.getElementById('duration');
  movie.duration = durationInput.value;
  const directorInput = document.getElementById('director');
  movie.director = directorInput.value;
  const descriptionInput = document.getElementById('description');
  movie.description = descriptionInput.value;
  const posterInput = document.getElementById('poster');
  movie.poster = posterInput.value;
  const ratingInput = document.getElementById('rating');
  movie.rating = ratingInput.value;

  // Suppression de la liste de films
  const movieSection = document.querySelector('.movie-section');
  movieSection.innerHTML = '';

  // Appel de la fonction pour afficher la liste complète
  displayMovies();
};

// Appel de la function displayMovie qui affiche les films
displayMovies()

// Appel de la fonction au moment de Submit
form.addEventListener("submit", submitForm);
